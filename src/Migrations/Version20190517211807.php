<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190517211807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(50) NOT NULL, bio LONGTEXT DEFAULT NULL, reset_pass_token VARCHAR(65) DEFAULT NULL, home_page VARCHAR(50) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE guest_response (id INT AUTO_INCREMENT NOT NULL, user_id_id INT DEFAULT NULL, user_name VARCHAR(50) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, home_page VARCHAR(50) DEFAULT NULL, status VARCHAR(30) DEFAULT \'pending\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, image VARCHAR(150) DEFAULT NULL, ip VARCHAR(15) NOT NULL, useragent VARCHAR(255) NOT NULL, INDEX IDX_4151C3849D86650F (user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content (id INT AUTO_INCREMENT NOT NULL, response_id_id INT NOT NULL, text LONGTEXT NOT NULL, locale VARCHAR(4) NOT NULL, excerpt VARCHAR(200) NOT NULL, INDEX IDX_FEC530A96F324507 (response_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE guest_response ADD CONSTRAINT FK_4151C3849D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A96F324507 FOREIGN KEY (response_id_id) REFERENCES guest_response (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE guest_response DROP FOREIGN KEY FK_4151C3849D86650F');
        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A96F324507');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE guest_response');
        $this->addSql('DROP TABLE content');
    }
}
