<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Captcha\Bundle\CaptchaBundle\Validator\Constraints\ValidCaptcha;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuestResponseRepository")
 * @ORM\HasLifecycleCallbacks
 */
class GuestResponse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @Assert\Regex("/^[a-zA-Z0-9]{3,50}$/")
     */
    private $user_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $home_page;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Content", mappedBy="response_id", orphanRemoval=true)
     */
    private $contents;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="guestResponses")
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=30, options={"default" : "pending"})
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $image;

    /**
   * @ValidCaptcha(
   *      message = "CAPTCHA validation failed, try again."
   * )
   */
    protected $captcha;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $useragent;

    public function __construct()
    {
        $this->contents = new ArrayCollection();
        $this->status = 'pending';
    }

    public function getCaptcha()
    {
        return $this->captcha;
    }

    public function setCaptcha($captcha)
    {
        $this->captcha = $captcha;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserName(): ?string
    {
        return $this->user_name;
    }

    public function setUserName(?string $user_name): self
    {
        $this->user_name = $user_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHomePage(): ?string
    {
        return $this->home_page;
    }

    public function setHomePage(?string $home_page): self
    {
        $this->home_page = $home_page;

        return $this;
    }

    /**
     * @return Collection|Content[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(Content $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setResponseId($this);
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            // set the owning side to null (unless already changed)
            if ($content->getResponseId() === $this) {
                $content->setResponseId(null);
            }
        }

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_name = $user_id->getUsername();
        $this->email = $user_id->getEmail();
        $this->home_page = $user_id->getHomePage();

        $this->user_id = $user_id;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getAuthorName(): string
    {
        if (empty($this->user_id)) {
            return (string) $this->getUserName();
        }

        $user = $this->getUserId();

        return $user->getUsername();
    }

    public function getAuthorEmail(): string
    {
        if (empty($this->user_id)) {
            return (string) $this->getEmail();
        }

        $user = $this->getUserId();

        return $user->getEmail();
    }

    public function getLocaleContent(string $locale): ?Content
    {
        foreach ($this->getContents() as $content) {
            if ($content->getLocale() == $locale) {
                return $content;
            }
        }

        return null;
    }

    public function getLocaleExcerpt(string $locale): string
    {
        foreach ($this->getContents() as $content) {
            if ($content->getLocale() == $locale) {
                return (string) $content->getExcerpt();
            }
        }

        return '';
    }

    public function getLocaleText(string $locale): string
    {
        foreach ($this->getContents() as $content) {
            if ($content->getLocale() == $locale) {
                return (string) $content->getText();
            }
        }

        return '';
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function isUserCanModify(?User $user): bool
    {
        $author = $this->getUserId();

        return (!empty($user) && in_array('ROLE_ADMIN', $user->getRoles())) ||
        (!empty($user) && !empty($author) && $user->getId() === $author->getId());
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getUseragent(): ?string
    {
        return $this->useragent;
    }

    public function setUseragent(string $useragent): self
    {
        $this->useragent = $useragent;

        return $this;
    }
}
