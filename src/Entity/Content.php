<?php

namespace App\Entity;

use Exception;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContentRepository")
 */
class Content
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $locale;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GuestResponse", inversedBy="contents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $response_id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $excerpt;

    private $allowed_tags;

    public function __construct()
    {
        $this->allowed_tags = [];
    }

    public function getAllowedTags(): array
    {
        return $this->allowed_tags;
    }

    public function setAllowedTags(array $tags)
    {
        $this->allowed_tags = $tags;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    protected function isClosedTags(string $text): bool
    {
        $openTags = [];
        $closeTags = [];

        preg_match('/<\w+.*>/isU', $text, $openTags);
        preg_match('/<\/\w+>/isU', $text, $closeTags);

        return count($openTags) === count($closeTags);
    }

    public function setText(string $text): self
    {
        $implodedTags = implode('|', $this->getAllowedTags());
        $tagRegex = '/<\/?(?!'.$implodedTags.'|\/).*>/isU';

        if (!$this->isClosedTags($text)) {
            throw new Exception('Text has unclosed tags');
        }

        $this->text = preg_replace($tagRegex, '', $text);
        $this->excerpt = $this->prepareExcerpt($text);

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getResponseId(): ?GuestResponse
    {
        return $this->response_id;
    }

    public function setResponseId(?GuestResponse $response_id): self
    {
        $this->response_id = $response_id;

        return $this;
    }

    public function getExcerpt(): ?string
    {
        return $this->excerpt;
    }

    public function setExcerpt(string $excerpt): self
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    protected function prepareExcerpt(string $text): string
    {
        if (strlen($text) <= 100) {
            return $text;
        }

        $spaceIndex = strpos($text, ' ', 100);

        return substr($text, 0, $spaceIndex + 1);
    }
}
