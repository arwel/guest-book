<?php

namespace App\Admin;

use App\Abstractions\HasContentTrait;
use App\Abstractions\HasImageTrait;
use App\Entity\GuestResponse;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Ip;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class GuestResponseAdmin extends AbstractAdmin
{
    use HasContentTrait;
    use HasImageTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $package = new PathPackage('/', new EmptyVersionStrategy());
        $langs = $this->getConfigurationPool()->getContainer()->getParameter('langs', []);
        $subject = $formMapper->getAdmin()->getSubject();
        $rawContents = $subject->getContents();
        $image = $subject->getImage();
        $contents = [];

        foreach ($rawContents as $content) {
            $contents[$content->getLocale()] = $content->getText();
        }
        unset($rawContents);

        $contentBuilder = $formMapper->create('contents', FormType::class, [
            'by_reference' => false,
            'mapped' => false,
        ]);

        foreach ($langs as $key => $_) {
            $contentBuilder->add($key, TextareaType::class, [
                'mapped' => false,
                'data' => $contents[$key] ?? '',
            ]);
        }

        $formMapper
        ->add('user_name', TextType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Please enter a Username',
                ]),
                new Length([
                    'min' => 3,
                    'max' => 50,
                ]),
            ],
        ])
        ->add('email', EmailType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Please enter an email',
                ]),
                new Email(),
            ],
        ])
        ->add('home_page', UrlType::class, [
            'required' => false,
            'constraints' => [
                new Length([
                    'max' => 50,
                ]),
                new Url(),
            ],
        ])
        ->add($contentBuilder)
        ->add('image', FileType::class, [
            'mapped' => false,
            'required' => false,
            'help' => empty($image) ? '' : '<img src="'.$package->getUrl($image).'" width="150">',
            'constraints' => [
                new File([
                    'mimeTypes' => 'image/jpeg',
                ])
            ],
        ])
        ->add('status', ChoiceType::class, [
            'choices' => [
                'Pending' => 'pending',
                'Active' => 'active',
                'Disabled' => 'disabled',
            ],
            'constraints' => [
                new Choice([
                    'choices' => [
                        'pending',
                        'active',
                        'disabled',
                    ],
                ])
            ],
        ])
        ->add('useragent', TextType::class, [
            'constraints' => [
                new Length([
                    'max' => 50,
                ]),
            ],
        ])
        ->add('ip', TextType::class, [
            'constraints' => [
                new Ip(),
            ],
        ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('user_name');
        $datagridMapper->add('email');
        $datagridMapper->add('status');
        $datagridMapper->add('created_at');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('user_name');
        $listMapper->addIdentifier('email');
        $listMapper->addIdentifier('status');
        $listMapper->addIdentifier('created_at');
    }
    public function validatePassword($object, ExecutionContextInterface $context, $payload)
    {
        $value = $context->getValue();

        if (empty($value)) {
            return;
        }

        $errors = $context->getValidator()->validate($value, [
            new NotBlank([
                'message' => 'Please enter a password',
            ]),
            new Length([
                'min' => 5,
                'max' => 250,
            ]),
        ]);

        foreach ($errors as $error) {
            $context->addViolation($error);
        }
    }

    public function postPersist($object)
    {
        $this->updateContent($object);
        $this->uploadImage($object);
    }

    public function postUpdate($object)
    {
        $this->updateContent($object);
        $this->uploadImage($object);
    }

    protected function updateContent(GuestResponse $response)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $doctrine = $container->get('doctrine');
        $manager = $doctrine->getManager();
        $config = $container->getParameter('responses');
        $allowedTags = $config['allowed_tags'] ?? [];
        $contents = $this->getForm()->get('contents')->all();

        $this->storeContent($manager, $response, $contents, $allowedTags);
    }

    protected function uploadImage(GuestResponse $response)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $doctrine = $container->get('doctrine');
        $manager = $doctrine->getManager();
        $uploadDir = $container->getParameter('upload_dir');
        $uploadUrl = $container->getParameter('upload_url');

        $form = $this->getForm();
        $uploadedFile = $form->has('image') ? $form->get('image')->getData() : null;
        $uploadedFile && $this->storeImage($uploadedFile, $response, $uploadDir, $uploadUrl);

        $manager->persist($response);
        $manager->flush();
    }
}