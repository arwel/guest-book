<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->add('username', TextType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Please enter a Username',
                ]),
                new Length([
                    'min' => 3,
                    'max' => 50,
                ]),
            ],
        ])
        ->add('password', RepeatedType::class, [
            'mapped' => false,
            'required' => false,
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'first_options'  => [
                'required' => false,
                'label' => 'Password',

            ],
            'second_options' => [
                'required' => false,
                'label' => 'Repeat Password',
            ],
            'constraints' => [
                new Callback([
                    'callback' => [
                        $this,
                        'validatePassword',
                    ],
                ]),
            ],
        ])
        ->add('home_page', UrlType::class, [
            'required' => false,
            'constraints' => [
                new Length([
                    'max' => 50,
                ]),
                new Url()
            ],
        ])
        ->add('bio', TextareaType::class, [
            'required' => false,
        ])
        ->add('roles', ChoiceType::class, [
            'multiple' => true,
            'choices' => [
                'Admin' => 'ROLE_ADMIN',
                'User' => 'ROLE_USER',
                'Blocked' => 'ROLE_BLOCKED',
            ],
            'constraints' => [
                new Choice([
                    'multiple' => true,
                    'choices' => [
                        'ROLE_ADMIN',
                        'ROLE_USER',
                        'ROLE_BLOCKED',
                    ],
                ])
            ],
        ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
        $datagridMapper->add('email');
        $datagridMapper->add('created_at');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('username');
        $listMapper->addIdentifier('email');
        $listMapper->addIdentifier('roles');
        $listMapper->addIdentifier('created_at');
    }
    public function validatePassword($object, ExecutionContextInterface $context, $payload)
    {
        $value = $context->getValue();

        if (empty($value)) {
            return;
        }

        $errors = $context->getValidator()->validate($value, [
            new NotBlank([
                'message' => 'Please enter a password',
            ]),
            new Length([
                'min' => 5,
                'max' => 250,
            ]),
        ]);

        foreach ($errors as $error) {
            $context->addViolation($error);
        }
    }
}