<?php

namespace App\Abstractions;

use App\Entity\Content;
use App\Entity\GuestResponse;
use Doctrine\ORM\EntityManager;
use Exception;

trait HasContentTrait
{
    protected function storeContent(EntityManager $manager, GuestResponse $response, array $fields, array $allowedTags)
    {
        try {
            $existingContents = [];
            foreach ($response->getContents() as $content) {
                $existingContents[$content->getLocale()] = $content;
            }

            foreach ($fields as $lang => $value) {
                $content = $existingContents[$lang] ?? new Content();
                $content->setAllowedTags($allowedTags);
                $content->setLocale($lang);
                $content->setText($value->getData());
                $content->setResponseId($response);

                $manager->persist($content);
            }

            $manager->flush();
        } catch (Exception $e) {
            $this->addFlash('danger', $e->getMessage());
        }
    }
}