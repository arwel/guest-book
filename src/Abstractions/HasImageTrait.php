<?php

namespace App\Abstractions;

use App\Entity\GuestResponse;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait HasImageTrait
{
    protected function storeImage(UploadedFile $uploadedFile, GuestResponse $guestResponse, string $uploadDir, string $uploadUrl)
    {
        try {
            $datePath = implode(DIRECTORY_SEPARATOR, [
                date('Y'),
                date('m'),
                date('d')
            ]);
            $uploadDir = $uploadDir.DIRECTORY_SEPARATOR.$datePath;
            $uploadUrl = $uploadUrl.DIRECTORY_SEPARATOR.$datePath;
            $fileName = $uploadedFile->getClientOriginalName();

            if (!is_dir($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }

            while (is_file($uploadDir.DIRECTORY_SEPARATOR.$fileName)) {
                $fileName = microtime().$fileName;
            }

            $uploadedFile->move($uploadDir, $fileName);

            $guestResponse->setImage($uploadUrl.DIRECTORY_SEPARATOR.$fileName);
        } catch (Exception $e) {
            $this->addFlash('danger', $e->getMessage());
        }
    }
}
