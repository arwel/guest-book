<?php

namespace App\Repository;

use App\Entity\GuestResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @method GuestResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuestResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuestResponse[]    findAll()
 * @method GuestResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuestResponseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GuestResponse::class);
    }

    // /**
    //  * @return GuestResponse[] Returns an array of GuestResponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuestResponse
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getJoinedContentQueryBuilder(string $orderBy, string $orderType)
    {
        return $this->createQueryBuilder('g')
            ->innerJoin('g.contents', 'c')
            ->addSelect('c')
            ->orderBy('g.'.$orderBy, $orderType)
        ;

    }

    public function getQueryForLocaledContent(string $status, string $locale, string $orderBy, string $orderType)
    {
        return $this->getJoinedContentQueryBuilder($orderBy, $orderType)
            ->where('g.status = :status')
            ->setParameter('status', $status)
            ->andWhere('c.locale = :locale')
            ->setParameter('locale', $locale)
            ->getQuery()
        ;
    }

    public function findWithLocaledContent(string $status, string $locale, string $orderBy, string $orderType)
    {
        return $this->getQueryForLocaledContent($status, $locale, $orderBy, $orderType)
            ->getResult()
        ;
    }
}
