<?php

namespace App\DataFixtures;

use App\Entity\Content;
use App\Entity\GuestResponse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GuestResponseFixtures extends Fixture
{
    protected $httpClient;
    protected $langs;

    public function __construct(Client $httpClient, ContainerInterface $container)
    {
        $this->httpClient = $httpClient;
        $this->langs = $container->getParameter('langs');
    }

    public function load(ObjectManager $manager)
    {
        $data = $this->getDummyData();

        foreach ($data as $guest) {
            $response = new GuestResponse();
            $response->setEmail($guest['email'] ?? 'test@test.com');
            $response->setHomePage($guest['website'] ?? 'https://google.com');
            $response->setUserName($guest['username'] ?? 'test');
            $response->setIp('127.0.0.1');
            $response->setUseragent('Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/65.0');
            $response->setStatus('active');

            $manager->persist($response);

            foreach ($this->langs as $lang => $_) {
                $content = new Content();
                $content->setLocale($lang);
                $content->setText('LANG [' . $lang . ']  ' . implode(' ', $guest['company'] ?? []));
                $content->setResponseId($response);
                $manager->persist($content);
            }
        }

        $manager->flush();
    }

    protected function getDummyData(): array
    {
        try {
            $response = $this->httpClient->get('https://jsonplaceholder.typicode.com/users');

            if ($response->getStatusCode() !== 200) {
                throw new Exception('Incorrect status ' . $response->getStatusCode());
            }

            $content = json_decode($response->getBody()->getContents(), true);

            return is_array($content) ? $content : [];
        } catch (Exception $e) {
            return [];
        }
    }
}
