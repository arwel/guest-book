<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 20; $i++) {
            $name = 'user' . ($i + 1);

            $user = new User();
            $user->setUsername($name);
            $user->setEmail($name.'@gmail.com');
            $user->setHomePage('https://'.$name.'.com');
            $user->setBio($name.' BIO');
            $user->setPassword(
                $this->encoder->encodePassword(
                    $user,
                    $name
                )
            );

            if ($i === 0) {
                $user->setRoles(['ROLE_ADMIN']);
            }

            $manager->persist($user);
        }

        $manager->flush();
    }
}
