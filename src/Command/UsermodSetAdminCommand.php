<?php

namespace App\Command;

use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UsermodSetAdminCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'usermod:set-admin';
    protected $repo;
    protected $manager;


    public function __construct(ContainerInterface $container)
    {
        $doctrine = $container->get('doctrine');
        $this->repo = $doctrine->getRepository(User::class);
        $this->manager = $doctrine->getManager();

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add admin role to user by user email')
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');

        $user = $this->repo->findOneBy([
            'email' => $email,
        ]);

        if (empty($user)) {
            $io->error("Can not find user with email: {$email}");
            return;
        }

        $roles = $user->getRoles();
        $roles[] = 'ROLE_ADMIN';
        $user->setRoles($roles);

        $this->manager->persist($user);
        $this->manager->flush();

        $io->success("User with email {$email} is admin now");
    }
}
