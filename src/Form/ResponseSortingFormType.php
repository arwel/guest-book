<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class ResponseSortingFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('sort_by', ChoiceType::class, [
            'choices' => [
                'Date' => 'created_at',
                'Name' => 'user_name',
                'Email' => 'email',
            ],
            'constraints' => [
                new Choice([
                    'choices' => [
                        'user_name',
                        'email',
                        'created_at',
                    ],
                ])
            ],
            'attr' => [
                'class' => 'form-control'
            ],
        ])
        ->add('sort_type', ChoiceType::class, [
            'choices' => [
                'Desc' => 'desc',
                'Asc' => 'asc',
            ],
            'constraints' => [
                new Choice([
                    'choices' => [
                        'desc',
                        'asc',
                    ],
                ])
            ],
            'attr' => [
                'class' => 'form-control'
            ],
        ])
        ->add('sort', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-primary'
            ],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
