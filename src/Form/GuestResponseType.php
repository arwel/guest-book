<?php

namespace App\Form;

use App\Entity\GuestResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class GuestResponseType extends UserResponseType
{
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('user_name', TextType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Please enter a Username',
                ]),
                new Length([
                    'min' => 3,
                    'max' => 50,
                ]),
            ],
            'attr' => [
                'class' => 'form-control',
            ],
        ])
        ->add('email', EmailType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Please enter an email',
                ]),
                new Email(),
            ],
            'attr' => [
                'class' => 'form-control',
            ],
        ])
        ->add('home_page', UrlType::class, [
            'required' => false,
            'constraints' => [
                new Length([
                    'max' => 50,
                ]),
                new Url()
            ],
            'attr' => [
                'class' => 'form-control',
            ],
        ])
        ;

        parent::buildForm($builder, $options);
    }
}
