<?php

namespace App\Form;

use App\Entity\GuestResponse;
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserResponseType extends AbstractType
{
    protected $langs;

    public function __construct(ContainerInterface $container)
    {
        $this->langs = $container->getParameter('langs', []);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rawContents = $options['data']->getContents();
        $contents = [];

        foreach ($rawContents as $content) {
            $contents[$content->getLocale()] = $content->getText();
        }
        unset($rawContents);


        $contentBuilder = $builder->create('contents', FormType::class, [
            'by_reference' => false,
            'mapped' => false,
        ]);

        foreach ($this->langs as $key => $_) {
            $contentBuilder->add($key, TextareaType::class, [
                'mapped' => false,
                'required' => false,
                'data' => $contents[$key] ?? '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter an text',
                    ]),
                ],
            ]);
        }

        $builder
        ->add($contentBuilder)
        ->add('image', FileType::class, [
            'required' => false,
            'mapped' => false,
            'constraints' => [
                new File([
                    'mimeTypes' => 'image/jpeg',
                ])
            ],
            'attr' => [
                'class' => 'form-control',
            ],
        ])
        ->add('captcha', CaptchaType::class, [
            'captchaConfig' => 'ExampleCaptcha',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GuestResponse::class,
        ]);
    }
}
