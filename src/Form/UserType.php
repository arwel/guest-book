<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('username', TextType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Please enter a Username',
                ]),
                new Length([
                    'min' => 3,
                    'max' => 50,
                ]),
            ],
            'attr' => [
                'class' => 'form-control',
            ],
        ])
        ->add('password', RepeatedType::class, [
            'mapped' => false,
            'required' => false,
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'first_options'  => [
                'required' => false,
                'label' => 'Password',
                'attr' => [
                    'class' => 'form-control',
                ],
            ],
            'second_options' => [
                'required' => false,
                'label' => 'Repeat Password',
                'attr' => [
                    'class' => 'form-control',
                ],
            ],
            'constraints' => [
                new Callback([
                    'callback' => [
                        $this,
                        'validatePassword',
                    ],
                ]),
            ],
        ])
        ->add('home_page', UrlType::class, [
            'required' => false,
            'constraints' => [
                new Length([
                    'max' => 50,
                ]),
                new Url()
            ],
            'attr' => [
                'class' => 'form-control',
            ],
        ])
        ->add('bio', TextareaType::class, [
            'required' => false,
            'attr' => [
                'class' => 'form-control',
            ],
        ])

        ->add('save', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success mt-3',
            ],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    public function validatePassword($object, ExecutionContextInterface $context, $payload)
    {
        $value = $context->getValue();

        if (empty($value)) {
            return;
        }

        $errors = $context->getValidator()->validate($value, [
            new NotBlank([
                'message' => 'Please enter a password',
            ]),
            new Length([
                'min' => 5,
                'max' => 250,
            ]),
        ]);

        foreach ($errors as $error) {
            $context->addViolation($error);
        }
    }
}
