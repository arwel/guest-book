<?php

namespace App\Controller;

use App\Abstractions\HasContentTrait;
use App\Abstractions\HasImageTrait;
use App\Entity\Content;
use App\Entity\GuestResponse;
use App\Form\GuestResponseType;
use App\Form\ResponseSortingFormType;
use App\Form\UserResponseType;
use App\Repository\GuestResponseRepository;
use Doctrine\ORM\EntityManager;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class GuestResponseController extends AbstractController
{
    use HasContentTrait;
    use HasImageTrait;

    /**
     * @Route("/{page?1}", name="home", methods={"GET"}, requirements={"page"="\d+"})
     */
    public function index(
        $page = 1,
        GuestResponseRepository $repo,
        PaginatorInterface $paginator,
        ContainerInterface $container,
        Request $request,
        ValidatorInterface $validator
    ): Response
    {
        $orderBy = 'created_at';
        $orderType = 'desc';
        $sortingForm = $this->getSortingForm($request);

        if ($sortingForm->isSubmitted() && $sortingForm->isValid()) {
            $orderBy = $sortingForm->get('sort_by')->getData();
            $orderType = $sortingForm->get('sort_type')->getData();
        }

        $query = $repo->getQueryForLocaledContent('active', $request->getLocale(), $orderBy, $orderType);

        $config = $container->getParameter('responses');
        $items = (int) $config['items'] ?? 10;

        $responses = $paginator->paginate(
            $query,
            (int) $page,
            $items
        );

        return $this->render('guest_response/index.html.twig', [
            'guest_responses' => $responses,
            'is_own' => false,
            'sorting_form' => $sortingForm->createView(),
        ]);
    }

    /**
     * @Route("/my-responses/{page?1}", name="my_responses", methods={"GET"}, requirements={"page"="\d+"})
     */
    public function myResponsesList($page = 1,
        GuestResponseRepository $repo,
        PaginatorInterface $paginator,
        ContainerInterface $container,
        Request $request,
        ValidatorInterface $validator
    ): Response
    {
        $orderBy = 'created_at';
        $orderType = 'desc';
        $sortingForm = $this->getSortingForm($request);

        if ($sortingForm->isSubmitted() && $sortingForm->isValid()) {
            $orderBy = $sortingForm->get('sort_by')->getData();
            $orderType = $sortingForm->get('sort_type')->getData();
        }

        $query = $repo->getJoinedContentQueryBuilder($orderBy, $orderType)
        ->where('c.locale = :locale')
        ->setParameter('locale', $request->getLocale())
        ->andWhere('g.user_id = :user_id')
        ->setParameter('user_id', $this->getUser()->getId())
        ->getQuery();

        $config = $container->getParameter('responses');
        $items = (int) $config['items'] ?? 10;

        $responses = $paginator->paginate(
            $query,
            (int) $page,
            $items
        );

        return $this->render('guest_response/index.html.twig', [
            'guest_responses' => $responses,
            'is_own' => true,
            'sorting_form' => $sortingForm->createView(),
        ]);
    }

    /**
     * @Route("/new", name="guest_response_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $config = $this->getParameter('responses');
        $allowedTags = $config['allowed_tags'] ?? [];
        $currentUser = $this->getUser();
        $guestResponse = new GuestResponse();
        $form = $this->createForm(
            $currentUser ? UserResponseType::class : GuestResponseType::class,
            $guestResponse
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploadDir = $this->getParameter('upload_dir');
            $uploadUrl = $this->getParameter('upload_url');
            $uploadedFile = $form->has('image') ? $form->get('image')->getData() : null;
            $uploadedFile && $this->storeImage($uploadedFile, $guestResponse, $uploadDir, $uploadUrl);

            $currentUser && $guestResponse->setUserId($currentUser);

            $this->setResponseMetaData($request, $guestResponse);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($guestResponse);
            $manager->flush();

            $this->storeContent($manager, $guestResponse, $form->get('contents')->all(), $allowedTags);

            return $this->redirectToRoute('guest_response_edit', [
                'id' => $guestResponse->getId(),
            ]);
        }

        return $this->render('guest_response/new.html.twig', [
            'guest_response' => $guestResponse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="guest_response_edit", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function edit(Request $request, GuestResponse $guestResponse): Response
    {
        $config = $this->getParameter('responses');
        $allowedTags = $config['allowed_tags'] ?? [];
        $currentUser = $this->getUser();
        if (!$guestResponse->isUserCanModify($currentUser)) {
            $this->addFlash('danger', 'You can not edit this entity');

            return $this->redirectToRoute('home');
        }
        $form = $this->createForm(
            $currentUser ? UserResponseType::class : GuestResponseType::class,
            $guestResponse
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploadDir = $this->getParameter('upload_dir');
            $uploadUrl = $this->getParameter('upload_url');
            $manager = $this->getDoctrine()->getManager();

            $uploadedFile = $form->has('image') ? $form->get('image')->getData() : null;
            $uploadedFile && $this->storeImage($uploadedFile, $guestResponse, $uploadDir, $uploadUrl);

            $this->setResponseMetaData($request, $guestResponse);

            $manager->flush();
            $this->storeContent($manager, $guestResponse, $form->get('contents')->all(), $allowedTags);

            return $this->redirectToRoute('guest_response_edit', [
                'id' => $guestResponse->getId(),
            ]);
        }

        return $this->render('guest_response/edit.html.twig', [
            'guest_response' => $guestResponse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="guest_response_delete", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function delete(Request $request, GuestResponse $guestResponse): Response
    {
        if (!$guestResponse->isUserCanModify($this->getUser())) {
            $this->addFlash('danger', 'You can not remove this entity');

            return $this->redirectToRoute('home');
        }

        if ($this->isCsrfTokenValid('delete'.$guestResponse->getId(), $request->request->get('_token'))) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($guestResponse);
            $manager->flush();
        }

        return $this->redirectToRoute('home');
    }

    protected function getSortingForm(Request $request)
    {
        $form = $this->createForm(
            ResponseSortingFormType::class,
            [
                'sort_by' => $request->get('sort_by', 'date'),
                'sort_type' => $request->get('sort_type', 'desc'),
            ],
            [
                'method' => 'get',
                'csrf_protection' => false,
                'attr' => [
                    'class' => 'mb-1'
                ],
            ]
        );
        $form->handleRequest($request);

        return $form;
    }

    protected function setResponseMetaData(Request $request, GuestResponse $response)
    {
        $server = $request->server;
        $response->setIp($server->get('REMOTE_ADDR'));
        $response->setUseragent($server->get('HTTP_USER_AGENT'));
    }
}
