<?php

namespace App\Controller;

use Exception;
use Swift_Mailer;
use Swift_Message;
use App\Entity\User;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use App\Security\GuestBookAuthenticator;
use App\Form\ResetPasswordType;

class ForgotPasswordController extends AbstractController
{
    /**
     * @Route("/auth/forgot-password", name="forgot_password")
     */
    public function forgotPassword(Request $request, ValidatorInterface $validator, Swift_Mailer $mailer): Response
    {
        try {
            $email = $request->get('email');
            $emailConstraint = new Email();
            $validatorErrors = $validator->validate($email, $emailConstraint);

            if ($request->getMethod() === 'POST' && count($validatorErrors) === 0) {
                $user = $this->setupPasswordResetToken($email);
                $this->sendMail($mailer, $user);
                $this->addFlash('success', 'Check your mail');
            }
        } catch (NotFoundHttpException $e) {
            $this->addFlash('danger', $e->getMessage());
        } finally {
            return $this->render('forgot_password/index.html.twig');
        }
    }

    /**
     * @Route("/auth/reset-password/{token}", name="reset_password", requirements={"token" = "[a-zA-Z0-9]+"})
     */
    public function resetPassword(string $token, Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, GuestBookAuthenticator $authenticator): Response
    {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        try {
            $user = $this->findUserBy([
                'reset_pass_token' => $token,
            ]);

            if ($form->isSubmitted() && $form->isValid()) {
                $user = $this->setupNewUserPassword($user, $form, $passwordEncoder);
                $user->setResetPassToken(null);

                return $guardHandler->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $authenticator,
                    'main' // firewall name in security.yaml
                );
            }
        } catch (NotFoundHttpException $e) {
            $this->addFlash('danger', $e->getMessage());

            return $this->redirectToRoute('forgot_password');
        }

        return $this->render('forgot_password/reset.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function sendMail(Swift_Mailer $mailer, User $user): Response
    {
        $fromEmail = $_ENV['MAILER_SEND_FROM'] ?? '';

        $message = (new Swift_Message('Reset Password'))
        ->setFrom($fromEmail)
        ->setTo($user->getEmail())
        ->setBody(
            $this->renderView(
                'emails/reset-password.html.twig',
                ['token' => $user->getResetPassToken(),]
            ),
            'text/html'
        )
        ;

        $mailer->send($message);
    }

    protected function setupPasswordResetToken(string $userEmail): User
    {
        $user = $this->findUserBy([
            'email' => $userEmail,
        ]);
        $user->setResetPassToken($this->generateRandomToken());

        return $this->saveUser($user);
    }

    protected function setupNewUserPassword(User $user, Form $form, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $form->get('password')->getData()
            )
        );

        return $this->saveUser($user);
    }

    protected function findUserBy(array $criteria): User
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->findOneBy($criteria);

        if (empty($user)) {
            throw $this->createNotFoundException('Wrong user access data');
        }

        return $user;
    }

    protected function saveUser(User $user): User
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        return $user;
    }

    protected function generateRandomToken(): string
    {
        return sha1(random_bytes(64));
    }
}
