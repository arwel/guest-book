<?php

namespace App\Controller;

use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user/current", name="current_user")
     */
    public function current(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $currentUser = $this->getUser();
        $form = $this->createForm(UserType::class, $currentUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('password');
            $password && $currentUser->setPassword(
                $passwordEncoder->encodePassword(
                    $currentUser,
                    $password->getData()
                )
            );

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($currentUser);
            $manager->flush();
        }

        return $this->render('user/index.html.twig', [
            'user' => $currentUser,
            'form' => $form->createView(),
        ]);
    }
}
