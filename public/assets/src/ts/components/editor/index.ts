import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

export default (context = document) => {
    const editors = context.querySelectorAll('textarea');

    for (const editor of Array.from(editors)) {
        ClassicEditor
        .create(editor, {
            toolbar: [ 'bold', 'italic', 'link', 'blockQuote' ]
        })
        .catch(e => {
            console.log(e);
        });
    }
};