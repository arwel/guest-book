class LocaleSwitcher {
  protected readonly form;

  constructor (form) {
    this.form = form;

    form.addEventListener('change', this.formChangeHandler);
  }

  formChangeHandler = ({target}) => {
    document.location.href = target.value;
  }
}

export default (context = document) => {
  const switchers = context.querySelectorAll('[data-component="locale-switcher"]');

  for (const switcher of Array.from(switchers)) {
    new LocaleSwitcher(switcher);
  }
};