import 'babel-polyfill';
import 'unfetch/polyfill';

import editorComponent from './components/editor';
import localeSwitcherComponent from './components/locale-switcher'


document.addEventListener('DOMContentLoaded', () => {
    editorComponent();
    localeSwitcherComponent();
});