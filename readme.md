After install this app, don't foget specify next parameters in your .env file:
 - DATABASE_URL=mysql://root:@127.0.0.1:3306/guest_book (example of dsn string for database connection)
 - MAILER_URL=smtp://smtp.mailbox.foo?encryption=ssl&username=username@mailbox.foo&password=masterkey&auth_mode=plain&port=465 (example of swiftmailer configuration string)
 - MAILER_SEND_FROM=musername@mailbox.foo (specify source mail address)

After loading fixtures, you can give admin role for any user. Just run "./bin/console usermod:set-admin %user-email%".
Fixtures don't contain any images.

Credentials for loaded users:
 - emails "user[1-20]@gmail.com"
 - passes "user[1-20]"
